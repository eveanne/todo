import React from "react";
import TableTasks from "./componant/TableTasks";
import { useState, useEffect } from "react";
// import ReactDOM from "react-dom";

const App = () => {
  const [newTaskName, setNewTaskName] = useState("");
  const [tasks, setTasks] = useState(
    JSON.parse(localStorage.getItem("tasks")) || []
  );

  const handleChange = (e) => setNewTaskName(e.currentTarget.value);

  const handleSubmit = (e) => {
    e.preventDefault();
    const name = newTaskName;
    if (!name.length || typeof name !== "string")
      return alert("Ne doit pas etre vide");

    const id = new Date().getTime();

    setTasks([...tasks, { id, name }]);
    setNewTaskName("");
  };

  useEffect(() => localStorage.setItem("tasks", JSON.stringify(tasks)), [
    tasks,
  ]);

  return (
    <div>
      <h1>ToDoList</h1>
      <TableTasks tasks={tasks} setTasks={setTasks} />
      <form htmlFor="ajouter" onSubmit={handleSubmit}>
        <input
          id="name"
          type="text"
          value={newTaskName}
          placeholder="Ajouter une Tache"
          onChange={handleChange}
        />
        <button>Confirmer</button>
      </form>
    </div>
  );
};

export default App;
