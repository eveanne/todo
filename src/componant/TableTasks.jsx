import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import { Button } from "@material-ui/core";
import Select from "./Select";

const TableTasks = ({ tasks, setTasks }) => {
  console.log(tasks);
  const useStyles = makeStyles({
    table: {
      minWidth: 550,
    },
  });

  const classes = useStyles();

  if (!tasks.length) return null;

  const handleDelete = (id) => setTasks(tasks.filter((t) => t.id !== id));

  const handleChangeStatus = ({ target: { value: newStatus } }, id) => {
    return setTasks(
      tasks.map((task) => {
        if (task.id === id)
          return {
            ...task,
            status: newStatus,
          };
        return task;
      })
    );
  };

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Nom </TableCell>
            <TableCell align="right">Echeance</TableCell>
            <TableCell align="right">Status</TableCell>
            <TableCell align="right">Suppression</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {tasks.map((task) => (
            <TableRow key={task.id}>
              <TableCell component="th" scope="row">
                {task.name}
              </TableCell>
              <TableCell align="right">{task.deadline}</TableCell>
              <TableCell align="right">
                <Select
                  task={task}
                  onChange={(e) => handleChangeStatus(e, task.id)}
                />
              </TableCell>
              <TableCell align="right">
                <Button onClick={() => handleDelete(task.id)}>X</Button>
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
  // }
};

export default TableTasks;
