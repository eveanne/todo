import SelectMUI from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import { status as statusEnum } from "../constants/status";
const Select = ({ task, onChange }) => {
  const getOptions = () => {
    const options = [];
    for (const key in statusEnum) {
      options.push(
        <MenuItem value={statusEnum[key]}>{statusEnum[key]}</MenuItem>
      );
    }
    return options;
  };
  return (
    <SelectMUI
      labelId="demo-simple-select-label"
      id="demo-simple-select"
      value={
        // Object.values = tableau avec juste les valeurs de l'obect (status)
        Object.values(statusEnum).includes(task.status)
          ? task.status
          : statusEnum.A_FAIRE
      }
      onChange={onChange}
    >
      {getOptions()};
    </SelectMUI>
  );
};

export default Select;
